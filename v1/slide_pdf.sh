#!/bin/bash

for lang in ruby python; do
  rm -R slides-$lang slides
  mkdir slides
  ./slide_split.py $lang
  for slide in slides/*; do
    inkscape -z -A "$slide".pdf "$slide"
  done
  mv slides slides-$lang
done
