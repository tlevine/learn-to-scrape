from scraperwiki import scrape
from lxml.html import fromstring
from scraperwiki.sqlite import save
import re

URL="http://www.dol.gov/olms/regs/compliance/cba/Cba_CaCn.htm"

def slugify(text):
    return text

def getTableRows(tree):
    return []

def getRowText(tableRow):
    return []

html=scrape(URL)
print "HTML:"
print html
tree=fromstring(html)
print "LXML tree object:"
print tree

tableRows=getTableRows(tree)
print "Table rows:"
print tableRows

header=tableRows.pop(0)
headerText=getRowText(header)
columnNames=map(slugify,headerText)
print "Column Names"
print columnNames

for tableRow in tableRows:
    rowText=getRowText(tableRow)
    data=dict(zip(columnNames,rowText))
    print "One row of data:"
    print data
    save([],data)

