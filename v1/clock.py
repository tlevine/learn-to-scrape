from sys import argv
from time import sleep

minutes=int(argv[1])
seconds=60*minutes

seconds_so_far=0
def minutes_left():
  return seconds_left()/60

def seconds_left():
  return seconds-seconds_so_far

while minutes_left()>1:
  print "%d minutes left" % minutes_left()
  sleep(60)
  seconds_so_far+=60

while seconds_left()>0:
  sleep(1)
  seconds_so_far+=1
  print seconds_left()
