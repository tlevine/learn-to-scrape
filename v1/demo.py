#from scraperwiki import scrape
from lxml.html import fromstring
#from scraperwiki.sqlite import save

def scrape(url):
  import urllib2
  return urllib2.urlopen(url).read()

def save(a,b):
  print(b)

URL="http://www.nyc.gov/lobbyistsearch/search?lobbyist=After+School+Corporation+%28The%29+%2804%29"
COLUMNS=['principal','address','officer','client','additional','begin','end'] #,'details']
UNIQUE_COLUMNS=[]

def main():
  html=scrape(URL)
  tree=fromstring(html)
  tableRows=getTableRows(tree)
  for tableRow in tableRows:
    rowText=getRowText(tableRow)
    saveRowText(rowText)

def test():
  "We can ignore this"
  html=scrape(URL)
  tree=fromstring(html)
  tableRows=getTableRows(tree)
  print getRowText(tableRows[0])

def getTableRows(tree):
  return tree.cssselect('td.nomatch > table > tr')[2:-1]

def getRowText(row):
  return [td.text_content() for td in row.cssselect('td')][0:7]

def saveRowText(rowText):
  data=dict(zip(COLUMNS,rowText))
  save(UNIQUE_COLUMNS,data)

main()
#test()
