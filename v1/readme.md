This mess of a git repository has some interesting files related to stream C of ScraperWiki's data camp.

* tom_demonstration_scraper.{py,rb} is the scraper Tom will program in front of everyone.
* tom_demonstration.tex is an outline that tom will use during that demonstration
* schedule.csv is a temporal break-down of the three-hour tutorial.
* scraper_assignments.tex is where assignments for the latter half of the turorial will go.
* slides.svg are Tom's slides. We'll combine them with Julian's by converting both to PDF (!)

You can probably ignore the rest of the files.
