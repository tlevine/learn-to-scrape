#!/usr/bin/env python2

import lxml.html
x=lxml.html.parse('web/index.html')
x.xpath('//*/@time')                                                                         
minutes=sum(map(float,x.xpath('//*/@time')))
hours=minutes/60.0
print("%f hours are planned." % hours)
