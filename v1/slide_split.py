#!/usr/bin/env python2
import sys,os
import lxml.etree

LANGUAGE=sys.argv[1]
if LANGUAGE=="python":
  IGNORE_LANGUAGE="ruby"
elif LANGUAGE=="ruby":
  IGNORE_LANGUAGE="python"

NSMAP = {
  'sodipodi': 'http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd',
  'cc': 'http://web.resource.org/cc/',
  'svg': 'http://www.w3.org/2000/svg',
  'dc': 'http://purl.org/dc/elements/1.1/',
  'xlink': 'http://www.w3.org/1999/xlink',
  'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
  'inkscape': 'http://www.inkscape.org/namespaces/inkscape'
}

def main():
  x=lxml.etree.parse('slides.svg')
  layers=x.getroot().xpath('//svg:g[@inkscape:groupmode="layer" and @inkscape:label!="Background"]',namespaces=NSMAP)
  for layer in layers:
    layer.attrib['style']="display:none"

  for layer in layers:
    layername=layer.attrib['{http://www.inkscape.org/namespaces/inkscape}label']
    if IGNORE_LANGUAGE not in layername:
      layer.attrib['style']="display:inline;"
      s=lxml.etree.tostring(x).replace('{{language}}',LANGUAGE)
      f=open('slides/%s.svg' % layername,'w')
      f.write(s)
      f.close()
      layer.attrib['style']="display:none;"

main()
