require 'open-uri'
require 'scraperwiki'
require 'nokogiri'

URL="http://www.dol.gov/olms/regs/compliance/cba/Cba_CaCn.htm"

def slugify(text)
  return text
end

def getTableRows(tree)
  return []
end

def getRowText(tableRow)
  return []
end

html=open(URL)
puts "HTML:"
puts html
tree=Nokogiri::HTML(html)
puts "LXML tree object:"
puts tree

tableRows=getTableRows(tree)
puts "Table rows:"
puts tableRows

header=tableRows.shift
headerText=getRowText(header)
columnNames=headerText.map{|cellText| slugify(cellText)}
puts "Column Names"
puts columnNames

for tableRow in tableRows
  rowText=getRowText(tableRow)
  data=Hash[columnNames.zip(rowText)]
  puts "One row of data:"
  puts data
  ScraperWiki.save([],data)
end


