from lxml.html import fromstring,tostring
from urllib2 import urlopen

URL="http://www.nyc.gov/lobbyistsearch/search?lobbyist=After+School+Corporation+%28The%29+%2804%29"

def main():
  x=fromstring(urlopen(URL).read())
  table=x.cssselect('td.nomatch > table')[0]
  removeDetails(table)
  fixRows(table)
  fullPath(x)
  tableId(table)
  removeJunk(x)
  fixBody(x)
  open('lobbyists.html','w').write(tostring(x,pretty_print=True))
  delAttrib(x)
  open('lobbyists-table.html','w').write(tostring(table,pretty_print=True))

def tableId(table):
  table.attrib['id']='lobbyist-table'

def fixRows(table):
  for row in table.xpath('tr[position()=1 or position()=last()]'):
    table.remove(row)

def fullPath(x):
  for img in x.cssselect('img'):
    img.attrib['src']="http://www.nyc.gov/lobbyistsearch/"+img.attrib['src']
  for link in x.cssselect('link'):
    link.attrib['href']="http://www.nyc.gov/lobbyistsearch/"+link.attrib['href']

def removeDetails(table):
  details=table.xpath('descendant::td[table]')
  for detail in details:
    detail.getparent().remove(detail)

def removeJunk(x):
  junk=x.cssselect('form')+x.cssselect('script')
  for j in junk:
    j.getparent().remove(j)

def fixBody(x):
  body=x.cssselect('body')[0]
  body.attrib['background']="http://www.nyc.gov/lobbyistsearch/"+body.attrib['background']
  body.attrib['onload']=''

def delAttrib(x):
  for elt in x.cssselect('*'):
    for key in elt.attrib:
      del(elt.attrib[key])

main()
