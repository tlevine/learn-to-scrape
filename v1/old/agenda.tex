\documentclass{article}
\usepackage{hyperref}
%\usepackage{algorithmic}

\title{ScraperWiki Data Camp Scraping Tutorials}
\author{Thomas Levine}

\begin{document}
\maketitle

\newcommand\todo\emph

\section{Preparation}
Before the tutorial starts, we have, among other things, a list of sites that are easily scraped. If the sites are rather complicated for any reason, they are broken into tiny chunks so we can give each tiny chunk to a person/team. Such a tiny chunk might be
\begin{itemize}
  \item parsing some well-formed HTML to get the main data of interest
  \item parsing some well-formed HTML to get urls and request parameters to reach the data of interest
  \item performing bizarre HTTP requests and saving the raw resulting pages
\end{itemize}

\section{Agenda}
\begin{description}
  \item[2 minutes] Scraping makes you powerful. Get excited about scraping. (\autoref{sec:power})
  \item[8 minutes] \href{https://raw.github.com/tlevine/how-to-scrape/master/how-to-scrape.Pnw}{Structure of a scraper} (\autoref{sec:structure})
  \item[05 minutes] What is ScraperWiki, and how does it work? (\autoref{sec:scraperwiki})
  \item[15 minutes] Everyone watches the instructer write a scraper that scrapes one really simple webpage. This is like pair programming except that the whole class constitutes half of the pair. And I mean even simpler than \href{http://www.bom.gov.au/products/IDQ60901/IDQ60901.94580.shtml}{this webpage}. (\autoref{sec:pair-programming})
  \item[30 minutes] Everyone tries scraping one really simple webpage.
  \item[10 minutes] What is\footnote{ScraperWiki \href{https://bitbucket.org/ScraperWiki/scraperwiki/wiki/DataOrDatum}{proclaims} that data is singular} data? (\autoref{sec:data})
  \begin{itemize}
    \item \href{https://scraperwiki.com/scrapers/brisbane_current_weather3/}{Python}
    \item \href{https://scraperwiki.com/scrapers/brisbane_current_weather_1/}{PHP}
  \end{itemize}
  \item[5 minutes] Assign scraping chunks to different individuals or groups. (I haven't yet figured out whether or how to group them.) (\autoref{sec:assign-groups})
  \item[90 minutes\footnote{If we go over time in other parts of the tutorial, deduct the time from this part.}] Students scrape. (\autoref{sec:coding})
  \item[15 minutes] Students talk\ldots
%  \item[20 minutes] Students present their scrapers and discuss their approaches. If most students worked individually, we divide them into as many groups as we have instructors and present in different rooms. (\autoref{sec:presentations})
%  \item[25 minutes] Students and instructors discuss the various approaches that were used and the merits of each of the approaches. If they were divided in the previous block, the division continues. (\autoref{sec:presentations})
\end{description}
I could see the last two sections being shortened
because I don't want the presentations to get boring.

\subsection{Notable exclusions}
\begin{description}
  \item[SQL Statements] I haven't thought enough about what context this provides. Some things I have thought about
  \begin{itemize}
    \item People might already have alternatives to SQL (Excel, R, copy-paste)
    \item We could say `You can do all sorts of crazy awesome stuff with SQL once the data are in a relational database.'
      For example, it can do this: \texttt{[enormous query]}. We won't explain how it works,
      but we wanted to give you an idea of what was possible.'
  \end{itemize}
  \item[Parsing PDFs] They can be harder. But if we want to include them,
  let us instructors convert them to XML so we don't have to spend much time on them.
\end{description}

\section{Detailed outline}
\subsection{Introduction}
\subsubsection{Scraping makes you powerful}\label{sec:power}
\subsubsection{What is data?}\label{sec:data})
\todo{Julian will do this. Notes would be nice though.}

\subsection{About scrapers}
\subsubsection{Structure of a scraper}\label{sec:structure}
If we break scrapers down into smaller units, they are easier to think about and easier to write.
I think of scrapers as having two fundamental units\footnote{Typically, you also need a tiny bit of code to connect the two.}:
downloaders and parsers.
\begin{enumerate}
\item Downloaders retrieve raw files from the web (or some other external source) so we can manipulate them with scripts.
\item Once we've loaded the raw files into a script, parsers extract the data of interest.
\end{enumerate}
We're going to discuss how these two scraper building blocks can be combined to make very complex scrapers.

\paragraph{Parsing one page}
According to my framework, the simplest scrapers are those that load one page or parse one page.
Let's start with a one-page parser.
\begin{enumerate}
\item We've already downloaded a file and we want to extract some information from it
\item The file is a webpage, and it contains a table. We write a script that puts that table into a database.
\item Such a parser might be useful without a downloader.
For example, if you write files that you later covert to different formats,
it might be nice to have a script that converts them automatically.
\end{enumerate}

\paragraph{Downloading one page}
Now, how do we download a file?
\begin{enumerate}
\item Web pages are represented as a series of letters.
\item A simple downloader could download the webpage and put the letters somewhere.
\item A downloader could be quite useful without a parser.
One case is if you frequently download the same file from a private website;
instead of manually opening the website and clicking through login screens,
you can write a script that automatically logs you in and downloads the file.
\end{enumerate}

\paragraph{Downloading and parsing one page}
If you connect a one-page parser to a one-page downloader,
you can automatically download and parse a page.
For example, you could automatically download a webpage with a table
and convert that table to a spreadsheet.

But how do you send a downloader's output to a parser's input?
\begin{enumerate}
\item The obvious thing might be to save it to a file.
\item But using a database (like the ScraperWiki datastore) is sometimes nice.
\item We can also send the output directly to the parser without saving it anywhere.
This might not make sense to you if you haven't programmed before, so we'll discuss this more later.
Anyway, you should avoid doing this for anything but the simplest of scrapers
because it makes errors harder to fix.
\end{enumerate}

\paragraph{Downloading multiple pages}
\begin{itemize}
\item Write a function to download a page given a page identifier.
\item Apply it to each page with a loop or map
\end{itemize}

\paragraph{Parsing multiple pages}
\begin{itemize}
\item Write a function to download a page given a page identifier.
\item Apply it to each page with a loop or map
\end{itemize}

\paragraph{Two-level hierarchical scrapes}
What if you want to download and parse a lot of pages
but you can't get their urls by simply changing a number? 
In order to get a list of pages to download,
you'll often find yourself downloading another page
and parsing that page to find the list.

\paragraph{Hierarchical scrapes with more levels}
Maybe you're going through one page to get a list of pages,
then going to each of those pages to get a list of pages,
then going through each of those\ldots, and then finally
collecting some information from each of these pages.

You can sometimes write one function that works
on most of those levels, but this does get complex.

\subsubsection{ScraperWiki}\label{sec:scraperwiki}
\subsubsection{``Pair'' programming}\label{sec:pair-programming}

\subsection{Coding}

\subsubsection{Wacky table}
Now that we've talked way too much about scraping,
we want you to try it. First, we're all going work
on the same scraper. We're going to scrape this webpage.
\href{http://hacks.thomaslevine.com/wacky.html}

So go to ScraperWiki, make an account, and start writing
scapers for this table. You'll have to download the
webpage text, then parse it to put that table into the datastore.
And please tag your scrapers with ``wacky-table''.

\subsubsection{Real scrapers}
You're going to break into groups of two, and each group
is going to be assigned one data source to scrape.

In some cases, you'll all be working on different parts
of the same website, so we'll be able to connect your
scrapers once they've been written.

\paragraph{Assign groups}\label{sec:assign-groups}
We assign groups of two using an SQL query.
Or just based on where they're sitting;
walk around and tell pairs that they're a group.

\paragraph{Tasks}
%We're going to give each group two scraper projects.
%We suggest that you work on them together, sitting
%at the same computer, but you can also work on them
%more independently if you want.
%
Don't worry if you don't finish the two scrapers.
But if you do finish them, talk to us, and we can
give you trickier one!

There are about 30 students in the class.
Let's group them into twos. The same task is
given to two different groups.
So we need seven different tasks.

\paragraph{Go}\label{sec:coding}
Teachers go around and help people and stuff.

\subsection{Presentations}\label{sec:presentations}
%\subsection{Critique}


\section{Preparation things}
\subsection{Julian}
\begin{itemize}
\item Triage the data-to-be-liberated.
\item Chose one very simple example. Simplify it more and make it look like a webpage. Or Tom can do that part.
\item Break complex examples into simple bits.
\item Come up with at least seven simple bits.
\item Plan the SQL demonstration with the dumb example.
\end{itemize}
\subsection{Tom}
\begin{itemize}
\item Contact Lisa Williams about maybe running through with her.
\end{itemize}

\subsection{Aine}

\end{document}
