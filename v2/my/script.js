$(function() {
	$('.code').before('<div class="buttons">' +
      '<button class="python-button">Python</button>' +
      '<button class="ruby-button">Ruby</button></div>');
    sh_highlightDocument();
	$('body').addClass('python');

	$.deck('.slide');

	$('.python-button').click(function(){
		$('body').removeClass('ruby').addClass('python');
	});
	$('.ruby-button').click(function(){
		$('body').removeClass('python').addClass('ruby');
	});
});
